#! /bin/bash
#
cd /proj/${ROM_DIR} || exit 1
rm -rf /home/maestroci/.config
cp -r /proj/.config /home/maestroci
BUILD_START=$(date +"%s")
function sendTG() {
    curl -s "https://api.telegram.org/bot${TELEGRAM_TOKEN}/sendmessage" --data "text=${*}&chat_id=518221376&disable_web_page_preview=true&parse_mode=Markdown" > /dev/null
}

if [[ -n "$SYNC_SOURCE" ]]; then
    repo sync --force-sync --current-branch --no-tags --no-clone-bundle --optimized-fetch --prune -j$(nproc) -q
fi
. build/envsetup.sh
export ROOMSERVICE_BRANCHES="ten-plus"
export ROOMSERVICE_CLONE_DEPTH=1
lunch ${ROM_NAME}_${DEVICE}-user || sendTG "Lunching device $DEVICE failed!"
if [[ -n "${CLEAN_ALL}" ]]; then
    mka clobber
fi
sendTG "*Starting Build for ${ROM_NAME} for ${DEVICE}*%0A%0A*Runner:* $(hostname)%0A*Target:* $TARGET%0A*Time:* $(date +%T)"
CPUS=$(($(nproc) / 2))
make -j${CPUS} $TARGET
RESULT=$?
BUILD_END=$(date +"%s")
DIFF=$((BUILD_END - BUILD_START))
if [ "$RESULT" -ne 0 ]; then
    sendTG "*Build Failed.*%0A%0A*ROM: *${ROM_NAME}%0A*Device:* ${DEVICE}%0A*Runner:* $(hostname)%0A*Target:* $TARGET%0A%0A*BuildTime: *${DIFF}secs"
    exit 1
else
    rclone copy out/target/product/*$DEVICE*.zip onedrive-amrita:/Android-ROMs
    sendTG "*Build Success.*%0A%0A*ROM: *${ROM_NAME}%0A*Device:* ${DEVICE}%0A*Runner:* $(hostname)%0A*Target:* $TARGET%0A%*BuildTime: *${DIFF}secs.%0APushed to your OneDrive!"
fi